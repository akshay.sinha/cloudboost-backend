# Readme
## Round 1 Task - Backend
### Installation 
The dependencies can be installed by running the command `npm install`
### Execution
The code can be executed by running the command `npm start`
### Linting
Linter can be run by running `npm run lint`
### Testing
Testing can be performed with the command `npm test`
### Authentication
The login endpoint is located at `POST localhost:3000/login`. It uses JWT tokens for validating requests and accepts any combination of username and password. It returns a 400 error on empty strings.

**Example Request Body**
`{	username: "akshay", password: "password" }`

### JSON Patching
The json patch endpoint is located at `POST localhost:3000/jsonpatch`. It utilizes json-patch npm package to patch JSON objects. It returns 400 error on invalid inputs.

**Example Request Body**
`[{
"baz":  "qux",
"foo":  "bar"
},[
{ "op":  "replace", "path":  "/baz", "value":  "boo" },
{ "op":  "add", "path":  "/hello", "value": ["world"] },
{ "op":  "remove", "path":  "/foo"}
]
];`

### Thumbnail
The thumbnail endpoint can be found at `POST localhost:3000/thumbnail`. It utilizes the sharp npm package. Returns 400 on invalid urls.

**Example Request Body**
`{ url:  'https://jpeg.org/images/jpeg-logo.png'}`

### Dockerize
I've included the Dockerfile with the appropriate instructions. However, while running the container, I get an error from the image module sharp `Error: /app/node_modules/sharp/build/Release/sharp.node: invalid ELF header`. Unfortunately, I'm able to rectify this error!