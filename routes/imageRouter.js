'use strict';

const express = require('express');
const request = require('request').defaults({ encoding: null });
const sharp = require('sharp');
const authenticate = require('../authenticate');

const imageRouter = express.Router();

imageRouter.post('/thumbnail', authenticate.verifyUser,
  function(req, res, next) {
    if (req.body.url) {
      let url = req.body.url;
      request.get(url, (error, response, body) => { // eslint-disable-line
        sharp(body)
          .resize(50, 50, {
            kernel: sharp.kernel.nearest,
          })
          .background({r: 0, g: 0, b: 0, alpha: 0})
          .embed()
          .toBuffer()
          .then((data) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', response.headers['content-type']);
            res.end(data, 'binary');
          })
          .catch((err) => {
            res.statusCode = 400;
            res.setHeader('Content-Type', 'application/json');
            res.json({ error: err.message});
          });
      });
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ error: 'Must specify url!'});
    }
  });

module.exports = imageRouter;
