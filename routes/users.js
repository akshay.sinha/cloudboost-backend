'use strict';

var express = require('express');
var router = express.Router();

const authenticate = require('../authenticate');

router.post('/login', function(req, res, next) {
  if (req.body.username && req.body.password) {
    let user = {};

    user.username = req.body.username;
    user.password = req.body.password;

    let token = authenticate.getToken({ username: user.username });
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json({success: true, token: token,
      status: 'You are successfully logged in!'});
  } else {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'application/json');
    res.json({ error: 'Fields cannot be empty!'});
  }
});

module.exports = router;
