'use strict';

const express = require('express');
const jsonpatch = require('json-patch');
const authenticate = require('../authenticate');

const jsonRouter = express.Router();

jsonRouter.post('/jsonpatch', authenticate.verifyUser,
  function(req, res, next) {
    if (req.body[0] && req.body[1]) {
      let jsonObj = req.body[0];
      let patchArray = req.body[1];

      try {
        jsonpatch.apply(jsonObj, patchArray);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(jsonObj);
      } catch (err) {
        res.statusCode = 400;
        res.setHeader('Content-Type', 'application/json');
        res.json(err);
      }
    } else {
      res.statusCode = 400;
      res.setHeader('Content-Type', 'application/json');
      res.json({ error: 'Both objects must be defined in an array!'});
    }
  });

module.exports = jsonRouter;
