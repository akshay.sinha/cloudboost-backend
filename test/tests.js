/* eslint-disable */

process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

const www = require('../bin/www');

chai.use(chaiHttp);

describe('Login', () => {
  /*
   * Test the /POST route
   */ 
  describe('/POST login', () => {
    it('it should return a token', (done) => {
      let login = {
        username: "akshay",
        password: "password"
      };

      chai.request(www)
          .post('/login')
          .send(login)
          .end((err, res) => {
            res.should.have.status(200);            
            res.body.should.be.a('object');
            res.body.should.have.property('token');
            done();
          });
    });

    it('it should not return a token without username', (done) => {
      let login = {
        username: "",
        password: "password"
      };

      chai.request(www)
          .post('/login')
          .send(login)
          .end((err, res) => {
            res.should.have.status(400);            
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            done();
          });
    });

    it('it should not return a token without password', (done) => {
      let login = {
        username: "akshay",
        password: ""
      };

      chai.request(www)
          .post('/login')
          .send(login)
          .end((err, res) => {
            res.should.have.status(400);            
            res.body.should.be.a('object');
            res.body.should.have.property('error');
            done();
          });
    });
  });
});

describe('JSONPatch', () => {
  var token = null;

  before((done) => {
    let login = {
      username: "akshay",
      password: "password"
    };

    chai.request(www)
        .post('/login')
        .send(login)
        .end((err, res) => {
          token = res.body.token;
          done();
        });
  });

  /*
   * Test the /POST route
   */ 
  describe('/POST jsonpatch', () => {
    it('it should return the correct patched object', (done) => {
      let array = [{
        "baz": "qux",
        "foo": "bar"
      },
      [
        { "op": "replace", "path": "/baz", "value": "boo" },
        { "op": "add", "path": "/hello", "value": ["world"] },
        { "op": "remove", "path": "/foo"}
      ]
      ];

      chai.request(www)
          .post('/jsonpatch')
          .set('Authorization', `Bearer ${token}`)
          .send(array)
          .end((err, res) => {
            res.should.have.status(200);            
            res.body.should.be.a('object');
            res.body.should.have.property('baz').equal('boo');            
            res.body.should.have.property('hello');
            res.body.hello.should.be.a('array');
            done();
          });
    });

    it('it should return an error due to incorrect patch object', (done) => {
      let array = [{
        "baz": "qux",
        "foo": "bar"
      },
      [
        { "op": "replace", "path": "/baz", "value": "boo" },
        { "op": "addd", "path": "/hello", "value": ["world"] },
        { "op": "remove", "path": "/foo"}
      ]
      ];

      chai.request(www)
          .post('/jsonpatch')
          .set('Authorization', `Bearer ${token}`)
          .send(array)
          .end((err, res) => {
            res.should.have.status(400);            
            res.body.should.be.a('object');
            res.body.should.have.property('message')
              .equal('invalid operation: addd');            
            done();
          });
    });
  });  
});


describe('Thumbnail', () => {
  var token = null;

  before((done) => {
    let login = {
      username: "akshay",
      password: "password"
    };

    chai.request(www)
        .post('/login')
        .send(login)
        .end((err, res) => {
          token = res.body.token;
          done();
        });
  });

  /*
   * Test the /POST route
   */ 
  describe('/POST thumbnail', () => {
    it('it should return an image', (done) => {
      let url = { url: 'https://jpeg.org/images/jpeg-logo.png' };

      chai.request(www)
          .post('/thumbnail')
          .set('Authorization', `Bearer ${token}`)
          .send(url)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
    });

    it('it should return an error as no image was provided', (done) => {
      let url = { url: 'https://www.google.co.in' };

      chai.request(www)
          .post('/thumbnail')
          .set('Authorization', `Bearer ${token}`)
          .send(url)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have.property('error');
            done();
          });
    });

    it('it should return an error as url was empty', (done) => {
      let url = { url: '' };

      chai.request(www)
          .post('/thumbnail')
          .set('Authorization', `Bearer ${token}`)
          .send(url)
          .end((err, res) => {
            res.should.have.status(400);
            res.body.should.have.property('error');
            done();
          });
    });
  });  
});