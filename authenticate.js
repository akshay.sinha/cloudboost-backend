'use strict';

const passport = require('passport');
const ExtractJwt = require('passport-jwt').ExtractJwt;
const JwtStrategy = require('passport-jwt').Strategy;
const jwt = require('jsonwebtoken');

// const User = require('./models/user');
const config = require('./config');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.secretKey;

exports.getToken = function(user) {
  return jwt.sign(user, config.secretKey, {expiresIn: 3600});
};


exports.jwtPassport = passport.use(new JwtStrategy(opts,
  (jwt_payload, next) => {
    let user = {};
    user.username = jwt_payload.username;
    next(null, user);
  }));

exports.verifyUser = passport.authenticate('jwt', {session: false});
